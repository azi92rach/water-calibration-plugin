# !/usr/bin/python3
# -*- coding: utf-8 -*-

import water_calibration_plugin.settings as settings
import os
import numpy as np
from time import localtime, strftime

class WaterDataUtils:
    """ Tool to control the variables: save and load them. """

    def __init__(self):
        # Water calibration data
        if not os.path.exists(settings.WATER_PLUGIN_FILE_PATH): # create new vectors if the file not exists
            self.microLneeds = [np.array([]) for i in range(settings.TOT_PORTS)]
            self.results     = [np.array([]) for i in range(settings.TOT_PORTS)]
            self.grObtained  = [np.array([]) for i in range(settings.TOT_PORTS)]
            self.cylce       = np.array([])
            self.tolerance   = np.array([])
            self.board       = np.array([])
            self.date        = np.array([])
            self.stop        = np.array([])

        else:
            file = np.load(settings.WATER_PLUGIN_FILE_PATH) # Load the data from the file
            values = file.item()
            self.microLneeds = values['microLneeds']
            self.results     = values['results']
            self.grObtained  = values['grObtained']
            self.cylce       = values['cylce']
            self.tolerance   = values['tolerance']
            self.board       = values['board']
            self.date        = values['date']
            self.stop        = values['stop']


    def saveWaterResult(self, res, microL, gr, cycle, toler, board, stop=False):
        """ Add the data to the vector and save the results. """
        for i in range(settings.TOT_PORTS):
            self.microLneeds[i] = np.append(self.microLneeds[i], microL[i])
            self.results[i]     = np.append(self.results[i], res[i])
            self.grObtained[i]  = np.append(self.grObtained[i], gr[i])

        self.cylce     = np.append(self.cylce, cycle)
        self.tolerance = np.append(self.tolerance, toler)
        self.board     = np.append(self.board, board)
        self.date      = np.append(self.date, [self.__getTime()])
        self.stop      = np.append(self.stop, stop)
        # save data
        self.__saveData()

    def __saveData(self):
        """ Save the vectors into mat file"""
        #sio.savemat(setting.WATER_PLUGIN_FILE_PATH, )
        np.save(settings.WATER_PLUGIN_FILE_PATH, {'date':self.date, 'microLneeds': self.microLneeds, 'results':self.results,
                                                     'grObtained':self.grObtained, 'cylce': self.cylce,
                                                  'tolerance': self.tolerance, 'board': self.board, 'stop':self.stop})

    def getWaterResult(self):
        """ Get the vectors of data."""
        return [self.date, self.microLneeds, self.results, self.grObtained, self.cylce, self.tolerance, self.board, self.stop]

    def remove(self, idx):
        try:
            for i in range(settings.TOT_PORTS):
                self.microLneeds[i] = np.delete(self.microLneeds[i],idx)
                self.results[i]     = np.delete(self.results[i], idx)
                self.grObtained[i]  = np.delete(self.grObtained[i], idx)
            self.cylce = np.delete(self.cylce, idx)
            self.tolerance = np.delete(self.tolerance, idx)
            self.board = np.delete(self.board, idx)
            self.date = np.delete(self.date, idx)
            self.stop = np.delete(self.stop, idx)
            self.__saveData()
            return None
        except Exception as err:
            return err

    # Generate the timestamp
    def __getTime(self):
        return strftime("%d%b%Y_%H%M%S", localtime()) # get the time, useful to order the data